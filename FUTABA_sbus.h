#ifndef FUTABA_SBUS_H_
#define FUTABA_SBUS_H_

#define SERIAL_SBUS Serial1
#define RC_CHANS 16
#define SBUS_SYNCBYTE 0x0F // some sites say 0xF0

class FUTABA_SBUS{
private:
  uint16_t rcValue[RC_CHANS];
  unsigned int sbusIndex = 0;
  byte sbus[25];
  
public:
  FUTABA_SBUS();
  void init();
  void readSerial_RX();
  int16_t getData(uint8_t ch);
};

#endif

#include <Wire.h>
#include "FUTABA_sbus.h"

FUTABA_SBUS T10J;

void setup() {
  Serial.begin(9600);
  T10J.init();
}

void loop() {
  T10J.readSerial_RX();

  // setting to mode1
  int16_t roll = map(T10J.getData(0), 368, 1680, 1100, 1900);
  int16_t pitch = map(T10J.getData(1), 368, 1680, 1100, 1900);
  int16_t throttle = map(T10J.getData(2), 368, 1680, 1100, 1900);
  int16_t yaw = map(T10J.getData(3), 368, 1680, 1100, 1900);

  Serial.print(roll);
  Serial.print(", ");
  Serial.print(throttle);
  Serial.print(", ");
  Serial.print(pitch);
  Serial.print(", ");
  Serial.println(yaw);
  
  delay(500);

}

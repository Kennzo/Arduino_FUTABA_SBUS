#include <Arduino.h>
#include "FUTABA_sbus.h"
#include <Wire.h>

FUTABA_SBUS::FUTABA_SBUS() {
  for(int i = 0; i < RC_CHANS; i++) {
    rcValue[i] = 1500; //initialization
  }
  for(int i = 0; i < 25; i++) sbus[i] = 0;
}

void FUTABA_SBUS::init() {
  SERIAL_SBUS.begin(100000, SERIAL_8E2);
  
}

void FUTABA_SBUS::readSerial_RX() {
  while(SERIAL_SBUS.available()) {
    int val = SERIAL_SBUS.read();

    if(sbusIndex == 0 && val != SBUS_SYNCBYTE) {
      continue;
    }

    sbus[sbusIndex] = val;
    sbusIndex++;

    if (sbusIndex == 25) {
      sbusIndex = 0;

      if(sbus[24] != 0x0) {
        //11bit per 1Ch
        //368~1680 ?

        rcValue[0]   = ((sbus[1]     | sbus[2] << 8)  & 0x07FF);
        rcValue[1]   = ((sbus[2] >> 3  | sbus[3] << 5)  & 0x07FF);
        rcValue[2]   = ((sbus[3] >> 6  | sbus[4] << 2   | sbus[5] << 10) & 0x07FF);
        rcValue[3]   = ((sbus[5] >> 1  | sbus[6] << 7)  & 0x07FF);
        rcValue[4]   = ((sbus[6] >> 4  | sbus[7] << 4)  & 0x07FF);
        rcValue[5]   = ((sbus[7] >> 7  | sbus[8] << 1   | sbus[9] << 9) & 0x07FF);
        rcValue[6]   = ((sbus[9] >> 2  | sbus[10] << 6) & 0x07FF);
        rcValue[7]   = ((sbus[10] >> 5 | sbus[11] << 3) & 0x07FF);
        rcValue[8]   = ((sbus[12]    | sbus[13] << 8) & 0x07FF);
        rcValue[9]   = ((sbus[13] >> 3 | sbus[14] << 5) & 0x07FF);
        rcValue[10]  = ((sbus[14] >> 6 | sbus[15] << 2 | sbus[16] << 10) & 0x07FF);
        rcValue[11]  = ((sbus[16] >> 1 | sbus[17] << 7) & 0x07FF);
        rcValue[12]  = ((sbus[17] >> 4  | sbus[18] << 4)  & 0x07FF);
        rcValue[13]  = ((sbus[18] >> 7  | sbus[19] << 1   | sbus[20] << 9) & 0x07FF);
        rcValue[14]  = ((sbus[20] >> 2  | sbus[21] << 6) & 0x07FF);
        rcValue[15]  = ((sbus[21] >> 5 | sbus[22] << 3) & 0x07FF);

      }
    }
  }
}

int16_t FUTABA_SBUS::getData(uint8_t ch) {
  return rcValue[ch];
}

